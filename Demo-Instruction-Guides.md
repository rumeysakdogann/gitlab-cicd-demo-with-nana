# Gitlab CI/CD Pipeline project for Python Flask App
[*Project Source*](https://www.youtube.com/watch?v=qP8kir2GUgo&t=1s&ab_channel=TechWorldwithNana)

![](images/demo-architecture.png)

Python App Source Code: https://github.com/benc-uk/python-demoapp   

- First clone the project from repository.

```bash
git clone https://gitlab.com/nanuchi/gitlab-cicd-crash-course
```

- We will use `make test` command to run the unit tests given under `src/app/tests/` directory.

![](images/tests-passed.png)

Note: If you are getting `TypeError: required field "lineno" missing from alias` after running `make test` command, go to  `requirements.txt` file under `src` directory and change ` pytest=6.2.5`

- This application runs on port 5000. Since I have another program running on port 5000, first I will change the PORT then run our application locally with `make run` command.

![](images/app-runs-locally.png)

- Go to browser localhost:5004(127.0.0.1:5004), you should be able to see application running.

![](images/demo-app-running-on-browser-1.png)
![](images/demo-app-running-on-browser-2.png)